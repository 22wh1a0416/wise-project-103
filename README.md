Authors:
Anusha V K (416), Kushala (417), Raina F (418), Yagnasri D (419), J Anusha (420)

Reference:
https://emojiterra.com/honeybee/

License:
This license lets others distribute, remix, adapt, and build upon your work, even commercially, as long as they credit you for the original creation. This is the most accommodating of licenses offered. Recommended for maximum dissemination and use of licensed materials.

Required python libraries and modules:
pygame, PIL (pillow), numpy, tkinter

The zip file attached above consists of the following files, 5 of which are required for the execution of code:
BeeBreeding.pdf - Problem statement
BeeBreeding_code.py - Final code
BeeFlying.gif - Artwork used in the final code (Intro)
Beehivebg.gif - Artwork used in the final code (Intro)
Beemusic.mp3 - Music used in the final code (Intro)
BeeflyingIntro.py - Code used to display the intro 
